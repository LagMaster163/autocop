import socket
from pickle import dumps
from time import sleep

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

SERVER_IP = '127.0.0.1'
PORT = 5558

class client(socket.socket):
    def __init__(self):
        socket.socket.__init__(self, socket.AF_INET, socket.SOCK_STREAM)
        self.private_key = rsa.generate_private_key(public_exponent=65537,key_size=2048,backend=default_backend())
        self.public_key = self.private_key.public_key()
        self.connect((SERVER_IP, PORT))
        self.handshake()
        print('connected to server')

    def handshake(self):
        self.server_public_key = serialization.load_pem_public_key(self.recv(4096),backend=default_backend())
        self.send(self.public_key.public_bytes(encoding=serialization.Encoding.PEM,format=serialization.PublicFormat.SubjectPublicKeyInfo))

    def crypt_send(self, b):
        self.send(self.server_public_key.encrypt(b,padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()),algorithm=hashes.SHA256(),label=None)))

    def login(self, username, password):
        self.send(b'LOGIN')
        sleep(0.5)
        self.crypt_send(dumps({'username':username, 'password':password}))

    def new_login(self, username, name, hashed_password, key):
        self.send(b'NEW_LOGIN')
        sleep(0.5)
        self.crypt_send(dumps({'username':username, 'hashed_password':hashed_password, 'name':name, 'key':key}))

if __name__ == '__main__':
    x = client()
