import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from multiprocessing import Process
from time import sleep
from pathlib import Path

import client
import settings

WINDOWS = True

global banner
global icon
banner = str(Path('./assets/banner.png').resolve())
icon = str(Path('./assets/icon.ico').resolve())

class App(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.resizable(False, False)
        self.title("Auto-Cop")

        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (MainPage, LoginPage, NewUserPage, KeyInfoPage, SettingsPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(LoginPage)

    def show_frame(self, cont):

        frame = self.frames[cont]
        try:
            frame.on_show()
        except AttributeError:
            pass
        frame.tkraise()

    def login(self):

        try:
            self.client = client.client()
        except ConnectionRefusedError:
            messagebox.showerror('Error', 'Cannot Connect to Server')
            #return
        self.user = settings.user(self.frames[LoginPage].usr_name.get())


class MainPage(tk.Frame):

    def __init__(self, parent, root):

        tk.Frame.__init__(self,parent)
        self.geo = '449x154'
        self.root = root

        self.img = tk.PhotoImage(file=banner)
        self.banner = tk.Label(self,image=self.img)
        self.banner.place(x=0,y=0)

        #self.new = tk.Label(self, text='Start Bot', fg='grey')
        #self.new.bind('<Enter>', lambda e: self.new.config(fg='black'))
        #self.new.bind('<Leave>', lambda e: self.new.config(fg='grey'))
        #self.new.bind('<Button-1>',lambda e: root.show_frame(NewUserPage))
        #self.new.place(x=155, y=38)

        self.run = tk.Button(self, text='Run Bot', command=lambda : print('hi'), relief='groove')
        self.run.place(y=38, x=5, width=437)

        self.rows = []

        self.rows.append(Row_Widget(self))
        self.rows.append(Row_Widget(self))

        self.bw = Bottom_Widget(self, root)

        if WINDOWS: self.run.config(bg='white', bd=1)

        self.place_widgets()

    def place_widgets(self):
        for e, i in enumerate(self.rows):
            i.place(e*29+67)
        self.bw.place(len(self.rows)*29 + 60)
        self.geo='449x'+str(96+len(self.rows)*29)
        self.root.geometry(self.geo)

    def add_row(self):
        self.rows.append(Row_Widget(self))
        self.place_widgets()

    def on_show(self):
        self.root.geometry(self.geo)




class LoginPage(tk.Frame):

    def __init__(self, parent, root):

        tk.Frame.__init__(self, parent)
        self.geo = '449x154'
        self.root = root

        self.img = tk.PhotoImage(file=banner)
        self.banner = tk.Label(self,image=self.img)
        self.banner.place(x=0,y=0)

        self.usr_name = Mod_Entry(self, text='Username')
        self.usr_name.bind('<Return>', lambda e: self.password.focus())
        self.usr_name.place(x=115, y=53, width=218)

        self.password = Password_Entry(self, text='Password')
        self.password.bind('<Return>', self.login)
        self.password.place(x=115, y=85, width=218)

        self.new = tk.Label(self, text='Create A New Account', fg='grey')
        self.new.bind('<Enter>', lambda e: self.new.config(fg='black'))
        self.new.bind('<Leave>', lambda e: self.new.config(fg='grey'))
        self.new.bind('<Button-1>',lambda e: root.show_frame(NewUserPage))
        self.new.place(x=157, y=117)

        self.incorrect_creds_warning = tk.Label(self, text='Your Username or Password is Incorrect', fg='red')
        self.no_username_warning = tk.Label(self, text='Please Enter Your Username', fg='red')
        self.no_password_warning = tk.Label(self, text='Please Enter Your Password', fg='red')



    def login(self,event):
        if not self.usr_name.act:
            self.reset_errs()
            self.new.place(x=157, y=128)
            self.no_username_warning.place(x=143, y=110)
        elif not self.password.act or self.password.get() == '':
            self.reset_errs()
            self.new.place(x=157, y=128)
            self.no_password_warning.place(x=145, y=110)
        else:
            try:
                self.root.login()
                self.root.show_frame(MainPage)
                self.reset_errs()
                self.new.place(x=157, y=117)
            except Exception as e:
                print(e)
                self.reset_errs()
                self.new.place(x=157, y=128)
                self.incorrect_creds_warning.place(x=114, y=110)

    def on_show(self):
        self.root.geometry(self.geo)

    def reset_errs(self):
        self.incorrect_creds_warning.place_forget()
        self.no_username_warning.place_forget()
        self.no_password_warning.place_forget()


class NewUserPage(tk.Frame):

    def __init__(self, parent, root):
        tk.Frame.__init__(self, parent)
        self.geo = '449x154'
        self.root = root

        self.img = tk.PhotoImage(file=banner)
        self.banner = tk.Label(self,image=self.img)
        self.banner.place(x=0,y=0)

        self.usr_name = Mod_Entry(self, text='Username')
        self.f_name = Mod_Entry(self, text='First Name')
        self.l_name = Mod_Entry(self, text='Last Name')

        x = 12

        self.usr_name.place(x=5, y=36+x, width=143)
        self.f_name.place(x=152, y=36+x, width=143)
        self.l_name.place(x=300, y=36+x, width=143)

        self.password = Password_Entry(self, text='Password')

        self.confirm = Password_Entry(self, text='Confirm Password')

        self.password.place(x=5, y=68+x, width=217)
        self.confirm.place(x=226, y=68+x, width=217)

        self.key = Mod_Entry(self, text='Activation Key')
        self.key.bind('<Return>', self.send_request)

        self.key.place(x=153, y=100+x, width=143)

        self.key_question = tk.Label(self, text='?', fg='grey')
        self.key_question.bind('<Enter>', lambda e: self.key_question.config(fg='black'))
        self.key_question.bind('<Leave>', lambda e: self.key_question.config(fg='grey'))
        self.key_question.bind('<Button-1>',lambda e: root.show_frame(KeyInfoPage))

        self.key_question.place(x=300, y=100+x)

        self.back = tk.Label(self, text='Back', fg='grey')
        self.back.bind('<Enter>', lambda e: self.back.config(fg='black'))
        self.back.bind('<Leave>', lambda e: self.back.config(fg='grey'))
        self.back.bind('<Button-1>',lambda e: root.show_frame(LoginPage))

        self.back.place(x=5, y=131)

    def send_request(self,event):
        print('send')


    def on_show(self):
        self.root.geometry(self.geo)

class KeyInfoPage(tk.Frame):

    def __init__(self, parent, root):
        tk.Frame.__init__(self, parent)
        self.geo = '449x154'
        self.root = root

        self.info = 'In order to use Auto-Cop you have to get an activation key. \nThis activation key will be given to you after the payment for the bot is complete. \nOnce you have the key then you can create an account.'
        self.text = tk.Label(self, text=self.info, justify=tk.LEFT)
        self.text.place(x=5, y=6)

        self.back = tk.Label(self, text='Back', fg='grey')
        self.back.bind('<Enter>', lambda e: self.back.config(fg='black'))
        self.back.bind('<Leave>', lambda e: self.back.config(fg='grey'))
        self.back.bind('<Button-1>',lambda e: root.show_frame(NewUserPage))

        self.back.place(x=5, y=131)

    def on_show(self):
        self.root.geometry(self.geo)

class SettingsPage(tk.Frame):

    def __init__(self, parent, root):
        tk.Frame.__init__(self, parent, bg='white')
        self.geo = '449x448'
        self.root = root

        self.browser = BrowserSettings(self, root)
        self.credsp = CredsSettings(self, root)
        self.captcha = CaptchaSettings(self, root)
        self.proxy = ProxySettings(self, root)

        self.browser.place(x=227,y=5)
        self.credsp.place(x=5,y=5)
        self.captcha.place(x=5,y=200)
        self.proxy.place(x=227,y=200)

        self.back = tk.Label(self, text='Back', fg='grey')
        self.back.bind('<Enter>', lambda e: self.back.config(fg='black'))
        self.back.bind('<Leave>', lambda e: self.back.config(fg='grey'))
        self.back.bind('<Button-1>',lambda e: root.show_frame(MainPage))

        self.back.place(x=5, y=400)

    def on_show(self):
        self.root.geometry(self.geo)

class BrowserSettings(tk.Frame):

    def __init__(self, parent, root):
        tk.Frame.__init__(self, parent, width=217, height=190)
        self.root=root

        self.label = tk.Frame(self, width=207, height=20, bg='white')
        self.label.place(x=5, y=5)
        self.label_text = tk.Label(self.label,text='Browser Settings', bg='white')
        self.label_text.place(x=103, y=10, anchor='center')

        self.default_browser = tk.StringVar()
        self.default_browser.set('Chrome')
        self.browser = tk.OptionMenu(self, self.default_browser, *['Chrome', 'FireFox', 'Safari', 'Edge'], command=self.browser_support_check)
        self.browser.config(indicatoron=0, bg='white',width=12, bd=1, relief='sunken')
        self.browser['menu'].config(bg='white')

        self.running_type = tk.StringVar()
        self.running_type.set('normal')
        self.normal = tk.Checkbutton(self, text="Normal", variable=self.running_type, onvalue='normal', offvalue=None, command=lambda: self.running_type.set('normal'))
        self.imageless = tk.Checkbutton(self, text="Imageless", variable=self.running_type, onvalue='imageless', offvalue=None, command=lambda: self.running_type.set('imageless'))
        self.headless = tk.Checkbutton(self, text="Headless", variable=self.running_type, onvalue='headless', offvalue=None, command=lambda: self.running_type.set('headless'))
        self.browser_support_check(self.default_browser.get())

        self.browser.place(x=35, y=30, height=25)

        self.normal.place(x=140, y=30)
        self.imageless.place(x=140, y=55)
        self.headless.place(x=140, y=80)

    def browser_support_check(self, var):
        print(var)
        if var == 'Chrome':
            self.running_type.set('normal')
            self.headless.config(state='disabled')

        elif var == 'FireFox' or var == 'Safari' or var == 'Edge':
            self.headless.config(state='normal')

class CredsSettings(tk.Frame):

    def __init__(self, parent, root):
        tk.Frame.__init__(self, parent, width=217, height=190)
        self.root=root
        self.parent = parent

        self.label = tk.Frame(self, width=207, height=20, bg='white')
        self.label.place(x=5, y=5)
        self.label_text = tk.Label(self.label,text='Billing/Shipping Info Settings', bg='white')
        self.label_text.place(x=103, y=10, anchor='center')

        self.card_number = Mod_Entry(self, text='Card Number')
        self.exp_date = Mod_Entry(self, text='mm/yy')
        self.cvv = Mod_Entry(self, text='cvv')

        self.name = Mod_Entry(self, text='Name')
        self.addr = Mod_Entry(self, text='Address')
        self.city = Mod_Entry(self, text='City')
        self.state_abv = Mod_Entry(self, text='State')
        self.zip = Mod_Entry(self, text='Zip Code')
        self.email = Mod_Entry(self, text='Email')
        self.telephone = Mod_Entry(self, text='Phone Number')

        self.new_f = tk.Frame(self, width=48, height=20, bg='white')
        self.new = tk.Label(self.new_f, text='New', fg='grey', bg='white')
        self.new.bind('<Enter>', lambda e: self.new.config(fg='black'))
        self.new.bind('<Leave>', lambda e: self.new.config(fg='grey'))
        self.new.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.new_f.place(x=5, y=35)
        self.new.place(x=24, y=10, anchor='center')

        self.open_f = tk.Frame(self, width=48, height=20, bg='white')
        self.open = tk.Label(self.open_f, text='Open', fg='grey', bg='white')
        self.open.bind('<Enter>', lambda e: self.open.config(fg='black'))
        self.open.bind('<Leave>', lambda e: self.open.config(fg='grey'))
        self.open.bind('<Button-1>',lambda e: self.open_file())
        self.open_f.place(x=58, y=35)
        self.open.place(x=24, y=10, anchor='center')

        self.save_f = tk.Frame(self, width=48, height=20, bg='white')
        self.save = tk.Label(self.save_f, text='Save', fg='grey', bg='white')
        self.save.bind('<Enter>', lambda e: self.save.config(fg='black'))
        self.save.bind('<Leave>', lambda e: self.save.config(fg='grey'))
        self.save.bind('<Button-1>',lambda e: self.save_file())
        self.save_f.place(x=111, y=35)
        self.save.place(x=24, y=10, anchor='center')

        self.default_f = tk.Frame(self, width=48, height=20, bg='white')
        self.default = tk.Label(self.default_f, text='Default', fg='grey', bg='white')
        self.default.bind('<Enter>', lambda e: self.default.config(fg='black'))
        self.default.bind('<Leave>', lambda e: self.default.config(fg='grey'))
        self.default.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.default_f.place(x=164, y=35)
        self.default.place(x=24, y=10, anchor='center')

        self.file = tk.Entry(self, relief='flat')
        self.file.place(x=5, y=60,  width=207)


        self.card_number.place(x=5, y=60-5+35, width=110)
        self.exp_date.place(x=120, y=60-5+35, width=45)
        self.cvv.place(x=170, y=60-5+35, width=42)

        self.name.place(x=5, y=80+35, width=70)
        self.addr.place(x=80, y=80+35, width=132)

        self.city.place(x=5, y=110-5+35, width=90)
        self.state_abv.place(x=100, y=110-5+35, width=45)
        self.zip.place(x=150, y=110-5+35, width=62)

        self.email.place(x=5, y=130+35, width=101)
        self.telephone.place(x=111, y=130+35, width=101)

    def open_file(self):

         self.open_filename = filedialog.askopenfilename(initialdir =self.root.user.local_dir+'\\creds' ,title = "Select file",filetypes = (("pickle files","*.pkl"),("all files","*.*")))

    def save_file(self):

        self.save_filename = filedialog.asksavefilename(initialdir =self.root.user.local_dir+'\\creds' ,title = "Select file",filetypes = (("pickle files","*.pkl"),("all files","*.*")))







class CaptchaSettings(tk.Frame):

    def __init__(self, parent, root):
        tk.Frame.__init__(self, parent, width=217, height=190)
        self.root=root

        self.label = tk.Frame(self, width=207, height=20, bg='white')
        self.label.place(x=5, y=5)
        self.label_text = tk.Label(self.label,text='Auto Captcha Settings', bg='white')
        self.label_text.place(x=103, y=10, anchor='center')

        self.two_captcha = Mod_Entry(self, text='Api Key')
        self.anti_captcha = Mod_Entry(self, text='Api Key')
        self.image_typerz = Mod_Entry(self, text='Api Key')
        self.solve_recaptcha = Mod_Entry(self, text='Api Key')
        self.captcha_decoder = Mod_Entry(self, text='Api Key')


        self.two_captcha_var = tk.IntVar()
        self.anti_captcha_var = tk.IntVar()
        self.image_typerz_var = tk.IntVar()
        self.solve_recaptcha_var = tk.IntVar()
        self.captcha_decoder_var = tk.IntVar()
        self.two_captcha_check = tk.Checkbutton(self, variable=self.two_captcha_var, text='2Captcha', fg='grey')
        self.anti_captcha_check = tk.Checkbutton(self, variable=self.anti_captcha_var, text='Anti-Captcha', fg='grey')
        self.image_typerz_check = tk.Checkbutton(self, variable=self.image_typerz_var, text='ImageTypers', fg='grey')
        self.solve_recaptcha_check = tk.Checkbutton(self, variable=self.solve_recaptcha_var, text='Solve-ReCaptcha', fg='grey')
        self.captcha_decoder_check = tk.Checkbutton(self, variable=self.captcha_decoder_var, text='Captcha-Decoder', fg='grey')

        self.two_captcha.place(x=5,y=65, width=89)
        self.two_captcha_check.place(x=95,y=65)
        self.anti_captcha.place(x=5,y=90, width=89)
        self.anti_captcha_check.place(x=95,y=90)
        self.image_typerz.place(x=5,y=115, width=89)
        self.image_typerz_check.place(x=95,y=115)
        self.solve_recaptcha.place(x=5,y=140, width=89)
        self.solve_recaptcha_check.place(x=95,y=140)
        self.captcha_decoder.place(x=5,y=165, width=89)
        self.captcha_decoder_check.place(x=95,y=165)


        self.default_f = tk.Frame(self, width=207, height=20, bg='white')
        self.default = tk.Label(self.default_f, text='Set To Default', fg='grey', bg='white')
        self.default.bind('<Enter>', lambda e: self.default.config(fg='black'))
        self.default.bind('<Leave>', lambda e: self.default.config(fg='grey'))
        self.default.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.default_f.place(x=5, y=35)
        self.default.place(x=103, y=10, anchor='center')





class ProxySettings(tk.Frame):

    def __init__(self, parent, root):
        # Add Check if banned
        # add check latency

        tk.Frame.__init__(self, parent, width=217, height=190)
        self.root=root

        self.label = tk.Frame(self, width=207, height=20, bg='white')
        self.label.place(x=5, y=5)
        self.label_text = tk.Label(self.label,text='Proxy/Checkout Settings', bg='white')
        self.label_text.place(x=103, y=10, anchor='center')


        self.new_f = tk.Frame(self, width=48, height=20, bg='white')
        self.new = tk.Label(self.new_f, text='New', fg='grey', bg='white')
        self.new.bind('<Enter>', lambda e: self.new.config(fg='black'))
        self.new.bind('<Leave>', lambda e: self.new.config(fg='grey'))
        self.new.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.new_f.place(x=5, y=35)
        self.new.place(x=24, y=10, anchor='center')

        self.open_f = tk.Frame(self, width=48, height=20, bg='white')
        self.open = tk.Label(self.open_f, text='Open', fg='grey', bg='white')
        self.open.bind('<Enter>', lambda e: self.open.config(fg='black'))
        self.open.bind('<Leave>', lambda e: self.open.config(fg='grey'))
        self.open.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.open_f.place(x=58, y=35)
        self.open.place(x=24, y=10, anchor='center')

        self.save_f = tk.Frame(self, width=48, height=20, bg='white')
        self.save = tk.Label(self.save_f, text='Save', fg='grey', bg='white')
        self.save.bind('<Enter>', lambda e: self.save.config(fg='black'))
        self.save.bind('<Leave>', lambda e: self.save.config(fg='grey'))
        self.save.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.save_f.place(x=111, y=35)
        self.save.place(x=24, y=10, anchor='center')

        self.default_f = tk.Frame(self, width=48, height=20, bg='white')
        self.default = tk.Label(self.default_f, text='Default', fg='grey', bg='white')
        self.default.bind('<Enter>', lambda e: self.default.config(fg='black'))
        self.default.bind('<Leave>', lambda e: self.default.config(fg='grey'))
        self.default.bind('<Button-1>',lambda e: root.show_frame(MainPage))
        self.default_f.place(x=164, y=35)
        self.default.place(x=24, y=10, anchor='center')

        self.file = tk.Entry(self, relief='flat')
        self.file.place(x=5, y=60,  width=207)


        self.prox_ip = Mod_Entry(self, text='Proxy Ip')
        self.prox_port = Mod_Entry(self, text='Proxy Port')
        self.prox_login = Mod_Entry(self, text='Proxy Login')
        self.prox_password = Mod_Entry(self, text='Proxy Password')
        self.use_var = tk.IntVar()
        self.use_prox = tk.Checkbutton(self, variable=self.use_var, text='Use Proxy', fg='grey')
        self.prox_ip.place(x=5, y=90, width=101)
        self.prox_port.place(x=111, y=90, width=101)
        self.prox_login.place(x=5, y=115, width=101)
        self.prox_password.place(x=111, y=115, width=101)
        self.use_prox.place(x=108, y=160, anchor='center')






class Row_Widget:

    def __init__(self, controller):

        self.controller = controller

        self.item_type_value = tk.StringVar(controller)
        self.item_type_value.set('Item Type')
        self.item_type = tk.OptionMenu(controller, self.item_type_value, *['jackets', 'shirts', 'tops_sweaters', 'sweatshirts', 'pants', 'shorts', 'hats', 'bags', 'accessories', 'skate'], command=lambda e: self.item_type.config(fg='black'))

        self.item_type.config(width=12)

        self.keywords = Mod_Entry(controller, text='Item Name')
        self.size = Mod_Entry(controller, text='Size')
        self.color = Mod_Entry(controller, text='Item Color')

        self.x = tk.Label(controller, text='X', fg='grey')
        self.x.config(font=("Helvetica", 13))
        self.x.bind('<Enter>', lambda e: self.x.config(fg='black'))
        self.x.bind('<Leave>', lambda e: self.x.config(fg='grey'))
        self.x.bind('<Button-1>',self.remove)

        if WINDOWS:
            self.item_type.config(indicatoron=0, bg='white',width=12, bd=1, relief='sunken', fg='grey')
            self.item_type['menu'].config(bg='white')

    def place(self, y):

        self.y = y

        self.item_type.place(x=5, y=y, height=25)
        self.keywords.place(x=97, y=y+2, width=150)
        self.size.place(x=254, y=y+2, width=65)
        self.color.place(x=327, y=y+2, width=90)
        self.x.place(x=422, y=y)

    def remove(self,*args):
        if len(self.controller.rows) > 1:
            self.item_type.destroy()
            self.keywords.destroy()
            self.size.destroy()
            self.color.destroy()
            self.x.destroy()
            try:
                self.controller.rows.remove(self)
                self.controller.place_widgets()
                del self
            except:
                pass

class Bottom_Widget:

    def __init__(self, controller, root):
        self.add = tk.Label(controller, text='+', fg='grey')
        self.add.config(font=("Courier", 22))
        self.add.bind('<Enter>', lambda e: self.add.config(fg='black'))
        self.add.bind('<Leave>', lambda e: self.add.config(fg='grey'))
        self.add.bind('<Button-1>',lambda e: controller.add_row())

        self.back = tk.Label(controller, text='Logout', fg='grey')
        self.back.bind('<Enter>', lambda e: self.back.config(fg='black'))
        self.back.bind('<Leave>', lambda e: self.back.config(fg='grey'))
        self.back.bind('<Button-1>',lambda e: root.show_frame(LoginPage))

        self.settings = tk.Label(controller, text='Setttings', fg='grey')
        self.settings.bind('<Enter>', lambda e: self.settings.config(fg='black'))
        self.settings.bind('<Leave>', lambda e: self.settings.config(fg='grey'))
        self.settings.bind('<Button-1>',lambda e: root.show_frame(SettingsPage))

        self.comunity = tk.Label(controller, text='Drop Schedule', fg='grey')
        self.comunity.bind('<Enter>', lambda e: self.comunity.config(fg='black'))
        self.comunity.bind('<Leave>', lambda e: self.comunity.config(fg='grey'))
        self.comunity.bind('<Button-1>',self.drop_schedule)

    def place(self, y):
        self.add.place(x=419, y=y)
        self.back.place(x=6, y=y+9)
        self.settings.place(x=60, y=y+9)
        self.comunity.place(x=123, y=y+9)

    def drop_schedule(self, event):
        pass

class Mod_Entry(tk.Entry):
    def __init__(self,root,text='',*args,**kwargs):
        self.text=text
        super(Mod_Entry, self).__init__(root, textvariable=tk.StringVar(root, value=self.text), *args, **kwargs)
        self.bind('<FocusIn>', self.on_entry_click)
        self.bind('<FocusOut>', self.on_focusout)
        self.config(fg = 'grey')
        self.act = False

    def on_entry_click(self,event):
        if self.get() == self.text:
           self.delete(0, "end")
           self.insert(0, '')
           self.config(fg = 'black')
           self.act = True

    def on_focusout(self,event):
        if self.get() == '':
            self.insert(0, self.text)
            self.config(fg = 'grey')
            self.act = False

class Password_Entry(Mod_Entry):
    def __init__(self,root,text='',*args,**kwargs):
        super(Password_Entry, self).__init__ (root,text=text,*args,**kwargs)

    def on_entry_click(self,event):
        if self.get() == self.text:
           self.delete(0, "end")
           self.insert(0, '')
           self.config(fg = 'black')
           self.config(show='•')
           self.act = True

    def on_focusout(self,event):
        if self.get() == '':
            self.insert(0, self.text)
            self.config(fg = 'grey')
            self.config(show='')
            self.act = False








app = App()
app.after(1, app.iconbitmap(icon))
app.mainloop()
