from pathlib import Path
import pickle
import json
from os import makedirs

class user:
    def __init__(self, username):
        self.username=username
        try:
            self.local_dir = str(Path('./users/'+self.username).resolve())
        except:
            makedirs(str(Path('./users/').resolve())+'\\'+username)
            self.local_dir = str(Path('./users/'+self.username).resolve())

        self.defaults={
            'shipping':
                {
                'file':''
                },
            'captcha':
                {
                '2cap':{'key':'', 'state':False},
                'anti':{'key':'', 'state':False},
                'image':{'key':'', 'state':False},
                'solve':{'key':'', 'state':False},
                'decode':{'key':'', 'state':False},
                },
            'browser':
                {
                'browser':'Chrome',
                'type':'Normal',
                'delay':3
                },
            'proxy':
                {
                'file':'',
                'use':False
                }
            }
        if not Path('./users/'+self.username+'/creds').exists():
            makedirs(self.local_dir+'\\creds')
        if not Path('./users/'+self.username+'/proxs').exists():
            makedirs(self.local_dir+'\\proxs')
        if not Path('./users/'+self.username+'/defaults.json').exists():
            self.export_defaults()

    def get_defaults(self):
        self.defaults=json.loads(open(self.local_dir + '\\defaults.json', 'r').read())

    def export_defaults(self):
        print(self.defaults)
        open(self.local_dir + '\\defaults.json', 'w').write(json.dumps(self.defaults))

    def new_creds(self, file, key, card_number='', exp_date='', cvv='', name='', addr='', city='', state_abv='', zip='', email='', tel=''):
        info = crypt.Encryption(pickle.dumps({'card_info':{'card_number':card_number, 'exp_date':exp_date, 'cvv':cvv}, 'cookie':{'value': '{name}%7C{addr}%7C%7C{city}%7C{state_abv}%7C{zip}%7CUSA%7C{email}%7C{phone}'.format(name=name.replace(' ','+'), addr=addr.replace(' ','+'), city=city, state_abv=state.upper(), zip=zip, email=email, phone=tel), 'path': '/', 'domain': 'www.supremenewyork.com', 'secure': False, 'name': 'address', 'httpOnly': False}}), key)
        open(self.local_dir+'\\creds\\'+file, 'w').write(pickle.dumps(info))

    def load_creds(self, file):
        self.crypt_creds = pickle.loads(open(self.local_dir+'\\creds\\'+file, 'r').read())

    def decrypt_creds(self, key):
        self.creds = self.crypt_creds.decrypt(key)

if __name__ == '__main__':
    x=user('a')
