import socket
import sys
from multiprocessing import Process
from pickle import loads


from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

class user_instance(Process):

    def __init__(self, conn):

        super().__init__()

        self.sock = conn
        self.private_key = rsa.generate_private_key(public_exponent=65537,key_size=2048,backend=default_backend())
        self.public_key = self.private_key.public_key()
        instance_type = self.sock.recv()
        if instance_type == b'LOGIN':
            try:
                self.handshake()
                self.get_creds()
            except:
                self.kill()

        elif instance_type == b'NEW_LOGIN':
            try:
                self.handshake()
                self.get_creds()
            except:
                self.kill()

    def handshake(self):
        self.sock.send(self.public_key.public_bytes(encoding=serialization.Encoding.PEM,format=serialization.PublicFormat.SubjectPublicKeyInfo))
        self.client_public_key = serialization.load_pem_public_key(self.sock.recv(4096),backend=default_backend())

    def crypt_recv(self, num=4096):
        self.private_key.decrypt(self.sock.recv(4096), padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()),algorithm=hashes.SHA256(),label=None))

    def get_creds(self):
            creds = loads(crypt_recv())
            self.username = creds['username']
            self.password = creds['password']

    def login(self, database_dir):

    def new_login(self, username, hash):


    def run(self):
        pass

    def kill(self):
        self.sock.close()
        self.terminate()

class keychain(Prosess):
    def __init__(self):
        super().__init__()
        self.



p = []

host = ''
port = 5558
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((host, port))
except socket.error as e:
    print(str(e))

s.listen()
print('Waiting for a connection.')

while True:

    conn, addr = s.accept()
    print('connected to: '+addr[0]+':'+str(addr[1]))

    p.append(user_instance(conn))




'''
user connects and server sends public key, client sends public key
user sends encrypted credentials, server compares them to database and then terminates any other instances of that user
then when the user requests the code, the server sends it
'''
